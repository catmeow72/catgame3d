extends Control

@onready var change_btn: Button = $ChangeBtn
@onready var amount_label: Label = $AmountLabel
@onready var amount_input: Range = $AmountInput
@onready var relative_input: BaseButton = $RelativeInput
@onready var max_input: BaseButton = $MaxInput
@onready var also_health_input: BaseButton = $AlsoHealthInput

var relative: bool:
	get:
		return !max && relative_input.button_pressed
@warning_ignore(shadowed_global_identifier)
var max: bool:
	get:
		return max_input.button_pressed
var current: bool:
	get:
		return !max || also_health_input.button_pressed

var flags: int: 
	get:
		return (0b01 if relative else 0) + (0b10 if max else 0)

var flags_prev: int = -1

var values: Dictionary = {
	0b00: 20,
	0b01: -1,
	0b10: 20,
}

var max_hp_value: int = 20
var relative_hp_value: int = -1
var absolute_hp_value: int = 20

# Called when the node enters the scene tree for the first time.
func _ready():
	_on_relative_input_toggled(relative_input.button_pressed)
	_on_max_input_toggled(max_input.button_pressed)
	_on_also_health_input_toggled(also_health_input.button_down)


func _on_change_hp_btn_pressed():
	var amount: int = int(amount_input.value)
	if max:
		Player.max_health = amount
	if current:
		if relative:
			Player.health += amount
		else:
			Player.health = amount


func _update():
	relative_input.visible = !max
	also_health_input.visible = max
	amount_label.text = "by:" if relative else "to:"
	change_btn.text = "Change " + ("max " if max else "") + "health"
	if flags_prev != -1:
		values[flags_prev] = amount_input.value
	if flags_prev != flags:
		amount_input.value = values[flags]
		flags_prev = flags
	

func _on_relative_input_toggled(_button_pressed):
	_update()

func _on_max_input_toggled(_button_pressed):
	_update()


func _on_also_health_input_toggled(_button_pressed):
	_update()

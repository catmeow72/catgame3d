class_name Entity extends StaticBody3D

var tool: VoxelTool
var blockpos: Vector3i
var data: Dictionary

var update_func: Callable

func update():
	tool.channel = VoxelBuffer.CHANNEL_TYPE
	tool.mode = VoxelTool.MODE_SET
	var meta = tool.get_voxel_metadata(blockpos)
	meta.data = data
	update_func.call(blockpos, meta)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


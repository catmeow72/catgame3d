extends Label

@export
var player: Node3D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_released("debug"):
		visible = !visible
	if visible:
		var plrpos := player.position
		var blockpos := Vector3i((plrpos + (Vector3.ONE * 0.5)).floor())
		var blockpos_transformed = RoomGenerator.transform_blockpos(blockpos)
		var roompos := RoomGenerator.get_roompos(blockpos_transformed)
		text = """FPS: %d
Position: %s
Block position: %s
Transformed block position: %s
Room position: %s""" % [Engine.get_frames_per_second(), plrpos.snapped(Vector3.ONE * 0.001), blockpos, blockpos_transformed, roompos]

extends HBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(10):
		var slot: HotbarSlot = HotbarSlot.new()
		slot.inventory = Player.inventory
		slot.slot_id = i
		add_child(slot)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var left = Input.is_action_just_released("slot_left")
	var right = Input.is_action_just_released("slot_right")
	if left:
		Player.hotbar_id -= 1
	if right:
		Player.hotbar_id += 1
	Player.hotbar_id = posmod(Player.hotbar_id, 10)

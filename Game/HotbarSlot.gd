class_name HotbarSlot extends InventorySlot

func _init():
	disabled = false
	mouse_hovering = false
	
func _process(_delta):
	mouse_hovering = Player.hotbar_id == slot_id
	queue_redraw()

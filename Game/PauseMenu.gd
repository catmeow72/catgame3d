extends Control

func _ready():
	get_tree().set_group("debug", "visible", OS.is_debug_build())

func set_paused(paused: bool):
	var tree = get_tree()
	tree.paused = paused
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE if tree.paused else Input.MOUSE_MODE_CAPTURED
	visible = paused
	change_menu("PauseMenu")

func toggle_pause():
	set_paused(!get_tree().paused)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("pause"):
		toggle_pause()

func _on_back_btn_pressed():
	set_paused(false)

func _on_quit_btn_pressed():
	get_tree().change_scene_to_file("res://MainMenu/MainMenu.tscn")

var current_menu: String = ""

func change_menu(id: String):
	var container = $CenterContainer2
	for i in container.find_children("", "Control", false, false):
		if i.name == id:
			i.show()
		else:
			i.hide()
	current_menu = id


func _on_dbg_btn_pressed():
	change_menu("DebugMenu")

func _notification(what):
	match what:
		NOTIFICATION_WM_WINDOW_FOCUS_OUT:
			set_paused(true)


func _on_unpause_frame_btn_pressed():
	var old_menu: String = current_menu
	set_paused(false)
	await get_tree().process_frame
	await get_tree().process_frame
	set_paused(true)
	change_menu(old_menu)

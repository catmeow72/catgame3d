extends CharacterBody3D

@onready var raycast: RayCast3D = $Camera3D/RayCast3D
@onready var camera: Camera3D = $Camera3D
@onready var low_raycast: RayCast3D = $RayCast3D

const SPEED = 5.0
const SPRINT_MULTIPLIER = 1.5
const CROUCH_MULTIPLIER = 0.5
const JUMP_VELOCITY = 7.5
@onready var last_valid_position: Vector3 = position
var movement_enabled = false
var box_mover: VoxelBoxMover = VoxelBoxMover.new()
@export var terrain: VoxelTerrain
const aabb = AABB(Vector3(-0.25, -0.875, -0.25), Vector3(0.5, 1.75, 0.5))

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var on_floor: bool = false
func _ready():
	Player.load_file()
	position = Player.position
	rotation.y = Player.rot.x
	camera.rotation.x = Player.rot.y

func _physics_process(delta):
	if not movement_enabled:
		if low_raycast.is_colliding():
			position = last_valid_position
			movement_enabled = true
	var crouching: bool = Input.is_action_pressed("crouch")
	# Add the gravity.
	if not on_floor:
		velocity.y -= gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and on_floor:
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("left", "right", "up", "down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
		if crouching && on_floor:
			velocity.x *= CROUCH_MULTIPLIER
			velocity.z *= CROUCH_MULTIPLIER
		elif Input.is_action_pressed("sprint"):
			velocity.x *= SPRINT_MULTIPLIER
			velocity.z *= SPRINT_MULTIPLIER
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	var movement = box_mover.get_motion(position + (Vector3.ONE * 0.5), velocity * delta, aabb, terrain)
	global_position += movement
	on_floor = is_zero_approx(box_mover.get_motion(position + (Vector3.ONE * 0.5), Vector3.DOWN, aabb, terrain).y)
	if Input.is_action_just_pressed("interact"):
		if raycast.is_colliding():
			var collider = raycast.get_collider()
			if collider.has_method("interact"):
				collider.interact()
	last_valid_position = position
	Player.position = position
	
func _unhandled_input(event):
	if event is InputEventMouseMotion && Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		var rot = event.relative / 200
		rotation.y = rotation.y - rot.x
		camera.rotation.x = clamp(camera.rotation.x - rot.y, deg_to_rad(-90), deg_to_rad(90))
		Player.rot.x = rotation.y
		Player.rot.y = camera.rotation.x

func _exit_tree():
	Player.save_file()

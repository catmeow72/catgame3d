extends SaveFile

var inventory: Inventory

@export
var hotbar_id: int
@export
var position: Vector3
@export
var rot: Vector2
@export
var max_health: int
@export
var health: int:
	get:
		return health
	set(value):
		health = clamp(value, 0, max_health)
@export
var max_mana: int
@export
var mana: int:
	get:
		return mana
	set(value):
		mana = clamp(value, 0, max_mana)

func _init():
	reset()
	automatic_mode = true
	basename = "player"

func _save_file():
	var items = inventory.items
	dict = {
		"item_count": inventory.size,
		"items": [
		],
	}
	for item in items:
		if item == null:
			item = Item.new()
		dict.items.append({
			"id": item.id,
			"count": item.count
		})

func _load_file():
	set_if_exists("item_count", inventory, "size")
	apply_if_exists("items", func(_key: String, value):
		for i in range(value.size()):
			var item = Item.new()
			if value[i].has_all(["id", "count"]):
				item.id = value[i].id
				item.count = value[i].count
			inventory.set_slot(i, item)
	)

func _reset():
	super()
	max_mana = 20
	mana = max_mana
	max_health = 20
	health = max_health
	inventory = Inventory.new()
	inventory.size = 10
	position = Vector3(0.5, 0.75, 1.5)
	rot = Vector2.ZERO
	hotbar_id = 0

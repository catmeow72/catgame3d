class_name RoomGenerator extends VoxelGeneratorScript

var FLOORS: Array[Floor] = []

var ROOMS: Array[Room] = []

const ROOMS_DIR = "res://Rooms/"
const FLOORS_DIR = "res://Floors/"
const channel: int = VoxelBuffer.CHANNEL_TYPE
var noise: FastNoise2 = FastNoise2.new()
@warning_ignore(shadowed_global_identifier)
var seed: int:
	get:
		return seed
	set(value):
		seed = value
		noise.seed = randi()
		noise.update_generator()

static func transform_blockpos(pos: Vector3i) -> Vector3i:
	var output := pos
	output.x -= Room.SIZE.x / 2
	output.z -= Room.SIZE.z / 2
	return output

static func get_roompos(blockpos: Vector3i) -> Vector3i:
	var output := blockpos
	output = Vector3i((Vector3(output) / Vector3(Room.SIZE)).floor())
	return output

func _init():
	FLOORS = ModLoader.floors
	ROOMS = ModLoader.rooms
	for room in ROOMS:
		room.channel = channel
	for y in range(FLOORS.size()):
		FLOORS[y].valid_rooms = ROOMS.filter(func(room):
			return room._valid(FLOORS[y], y)
		)

func _generate_block(out_buffer: VoxelBuffer, origin: Vector3i, lod: int):
	if lod != 0:
		return
	var size = Room.SIZE
	if origin.y < 0 || origin.y > FLOORS.size() * size.y:
		out_buffer.fill(Blocks["default:darkness"], channel)
	else:
		var out_size = out_buffer.get_size()
		var min_pos = transform_blockpos(origin)
		var roomcount = Vector3i((Vector3(out_size) / Vector3(size)).ceil())
		var rooms: Dictionary = {}
		var min_room_pos: Vector3i = get_roompos(min_pos)
		for x in range(roomcount.x + 1):
			for y in range(roomcount.y + 1):
				for z in range(roomcount.z + 1):
					var roompos: Vector3i = min_room_pos + Vector3i(x, y, z)
					var room: Room = choose_room(roompos)
					var belowroom: Room = choose_room(roompos - Vector3i(0, 1, 0))
					rooms[roompos] = {
						"room": room,
						"belowroom": belowroom,
					}
		var door_pos_x = size.x / 2
		var door_pos_z = size.z / 2
		for x in range(out_size.x):
			for y in range(out_size.y):
				for z in range(out_size.z):
					var pos: Vector3i = Vector3i(x, y, z) + min_pos
					var roompos: Vector3i = get_roompos(pos)
					if roompos.y >= FLOORS.size():
						out_buffer.set_voxel(Blocks["default:darkness"], x, y, z, channel)
						continue
					@warning_ignore(shadowed_global_identifier)
					var floor = FLOORS[roompos.y]
					var room: Room = rooms[roompos].room
					var belowroom: Room = rooms[roompos].belowroom
					pos -= roompos * size
					if pos.x < 0:
						pos.x += size.x
					if pos.y < 0:
						pos.y += size.y
					if pos.z < 0:
						pos.z += size.z
					var bufpos = Vector3i(x, y, z)
					if pos.y == size.y - 1:
						out_buffer.set_voxel_v(Blocks[floor.ceiling], bufpos, channel)
					elif pos.z == 0 || pos.x == 0:
						out_buffer.set_voxel_v(Blocks[floor.wall], bufpos, channel)
					room._pre_modify(out_buffer, floor, pos, bufpos)
					belowroom._modify_above(out_buffer, floor, pos, bufpos)
					if ((pos.x == door_pos_x) || (pos.z == door_pos_z)) && pos.y < 2:
						out_buffer.set_voxel_v(Blocks.air, bufpos, channel)
					room._post_modify(out_buffer, floor, pos, bufpos)
					ModLoader.run_hook("post_block_generate", [out_buffer, floor, pos, bufpos, channel])

func _get_used_channels_mask():
	return 1 << channel

var ROOM_BASE := Room.new()
var rng := RandomNumberGenerator.new()

func choose_room(pos: Vector3i) -> Room:
	if 0 <= pos.y && pos.y < FLOORS.size():
		@warning_ignore(shadowed_global_identifier)
		var floor = FLOORS[pos.y]
		var valid_rooms = floor.valid_rooms
		return valid_rooms[noise_range(pos, valid_rooms.size() - 1)]
	else:
		return ROOM_BASE

func noise_range(pos: Vector3i, p_max: int):
	rng.seed = hash([pos, seed])
	var value = rng.randi_range(0, p_max)
	return value

extends VoxelTerrain

var loaded_entities: Dictionary = {}
var entities: Dictionary = {}

@warning_ignore(shadowed_global_identifier)
var seed: int

func save():
	var dict := {
		"seed": seed
	}
	SaveFile.save_dict(dict, Saves.current_world + "save.dat")

func load_save():
	randomize()
	var player_file = SaveFile.load_dict(Saves.current_world + "save.dat")
	if player_file.is_empty():
		seed = randi()
		return
	seed = player_file.seed


# Called when the node enters the scene tree for the first time.
func _ready():
	entities = ModLoader.entities
	(mesher as VoxelMesherBlocky).library = ModLoader.voxel_lib
	var new_generator = RoomGenerator.new()
	load_save()
	new_generator.seed = seed
	stream = VoxelStreamRegionFiles.new()
	stream.directory = Saves.current_world
	stream.save_generator_output = true
	generator = new_generator

func _on_data_block_entered(info):
	var buffer: VoxelBuffer = info.get_voxels()
	var blockpos = info.get_position()
	loaded_entities[blockpos] = []
	buffer.for_each_voxel_metadata(func(pos: Vector3i, metadata):
		if buffer.get_voxel(pos.x, pos.y, pos.z) == Blocks.object:
			var entity_src = entities[metadata.id]
			var entity: Entity
			var uentity # Unchecked entity
			if entity_src is PackedScene:
				uentity = entity_src.instantiate()
			elif entity_src is GDScript:
				uentity = entity_src.new()
			if uentity is Entity:
				entity = uentity
				entity.position = data_block_to_voxel(info.get_position()) + pos
				entity.data = metadata.data
				entity.tool = get_voxel_tool()
				entity.blockpos = data_block_to_voxel(info.get_position()) + pos
				entity.update_func = update_entity
				add_child(entity)
				loaded_entities[blockpos].append(entity)
				update_entity(entity.blockpos, metadata)
	)

func update_entity(blockpos: Vector3i, meta: Dictionary):
	var tool = get_voxel_tool()
	tool.set_voxel_metadata(blockpos, meta.duplicate(true))
	tool.set_voxel(blockpos, Blocks.object)

func _exit_tree():
	save_modified_blocks()
	save()

func free_recursive(parent: Node):
	for child in parent.get_children():
		free_recursive(child)
	parent.queue_free()

func _on_block_unloaded(blockpos: Vector3i):
	if loaded_entities.has(blockpos):
		var entity_list: Array = loaded_entities[blockpos]
		for entity in entity_list:
			entity_list.erase(entity)
			if entity is Entity:
				for i in entity.find_children("", "Node3D", true, false):
					i.hide()
				remove_child(entity)
				free_recursive(entity)

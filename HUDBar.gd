@tool
class_name HUDBar extends TextureProgressBar

@export
var real_value: int = 20
@export_color_no_alpha
var base_hsv: Color = Color.RED
@export
var min_hsv: Vector3 = Vector3(0, 0.4, 0.25)
@export
var max_hsv: Vector3 = Vector3(1.0, 0.8, 1.0)
@export
var step_hsv: Vector3 = Vector3(30.0 / 360.0, 1.0 / 5.0, 1.0 / 16.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_tint(course_value: int):
	if course_value < 0:
		return Color.WHITE
	var actual_tint := Color(1, 1, 1)
	var c_hue: float = course_value * step_hsv.x
	var c_value: float = floorf(c_hue) * step_hsv.z
	var c_saturation: float = max_hsv.y - (floorf(c_value - wrapf(value, 0.0, max_hsv.z - min_hsv.z)) * step_hsv.y)
	c_hue = wrapf(c_hue, min_hsv.x, max_hsv.x)
	c_value = wrapf(max_hsv.z - c_value, min_hsv.z, max_hsv.z + 0.001)
	c_saturation = wrapf(c_saturation, min_hsv.y, max_hsv.y + 0.001)
	actual_tint = Color.from_hsv(c_hue, c_saturation, c_value)
	return actual_tint

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	@warning_ignore(integer_division, narrowing_conversion)
	var course_value: int = real_value / max_value
	@warning_ignore(narrowing_conversion)
	value = posmod(real_value, max_value)
	texture_under = preload("res://HealthBarBG.png") if course_value <= 0 else preload("res://HealthBarFG.png")
	tint_under = get_tint(course_value - 1)
	tint_progress = get_tint(course_value)

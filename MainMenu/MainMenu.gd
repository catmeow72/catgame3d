extends Control

@onready var world_list: ItemList = $%WorldList

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().paused = false



func _on_play_btn_pressed():
	_change_menu("WorldSelect")


func _on_quit_btn_pressed():
	get_tree().quit()


func _on_create_world_btn_pressed():
	_change_menu("WorldCreator")


func _on_continue_btn_pressed():
	Saves.load_world(world_list.get_item_metadata(world_list.get_selected_items()[0]).dirname)


func _hide_world_select():
	_change_menu("WorldSelect")


func _on_world_select_back_btn_pressed():
	_change_menu("MainMenu")

var world: Dictionary

func _on_delete_btn_pressed():
	world = world_list.get_item_metadata(world_list.get_selected_items()[0])
	$%WorldDeleteConfirmationLabel.text = "Are you sure you want do delete dungeon '%s'?
It will be lost forever!" % world.name
	_change_menu("WorldDeletionConfirmation")


func _change_menu(menu: String):
	assert(has_node(menu))
	RenderingServer.render_loop_enabled = false
	for child in get_children(true):
		if child.name == menu:
			child.show()
		else:
			child.hide()
	RenderingServer.render_loop_enabled = true


func _on_world_deletion_yes_btn_pressed():
	Saves.delete_world(world.dirname)
	_change_menu("WorldSelect")

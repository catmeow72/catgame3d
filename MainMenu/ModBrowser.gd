extends VBoxContainer


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_visibility_changed():
	for i in get_children():
		if !(i.name == "LoadingAnim"):
			remove_child(i)
			i.queue_free()
	if is_visible_in_tree():
		pass

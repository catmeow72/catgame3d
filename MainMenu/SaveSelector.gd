extends ItemList

@onready var loading_anim: Control = $LoadingAnim
var worlds: Array[Dictionary] = []
# Called when the node enters the scene tree for the first time.
func _ready():
	pass



func _on_visibility_changed():
	while loading_anim == null:
		await get_tree().process_frame
	if is_visible_in_tree():
		loading_anim.show()
		worlds = await Saves.get_worlds()
		clear()
		for world in worlds:
			var idx = add_item(world.name)
			set_item_metadata(idx, world)
		loading_anim.hide()

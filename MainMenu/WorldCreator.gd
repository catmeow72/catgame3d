extends PanelContainer

@onready var line_edit: LineEdit = $VBoxContainer/CenterContainer/VBoxContainer/LineEdit
@onready var dir_label: Label = $VBoxContainer/CenterContainer/VBoxContainer/Label
signal finished
var meta: Dictionary

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_line_edit_text_changed(new_text):
	meta.name = new_text
	meta.dirname = (meta.name as String).to_snake_case().replace("#%&{}\\<>*?/ $!'\":@+`|=", "_").lstrip("-._").rstrip("-._")
	dir_label.text = meta.dirname

func _on_visibility_changed():
	while line_edit == null || dir_label == null:
		await get_tree().process_frame
	meta = {
		"name": line_edit.placeholder_text,
		"dirname": ""
	}
	line_edit.text = meta.name
	_on_line_edit_text_changed(meta.name)


func _on_create_world_finish_btn_pressed():
	Saves.create_world(meta)
	finished.emit()

class_name SaveFile extends Node

var basename: String = "unspecified"
var automatic_mode: bool = true

var dict: Dictionary = {}
func apply_if_exists(key: String, function: Callable):
	if dict.has(key):
		function.call(key, dict[key])

func set_if_exists(key: String, obj: Object = self, obj_key: String = key):
	if dict.has(key):
		obj.set(obj_key, dict[key])

static func save_dict(p_dict: Dictionary, path: String) -> void:
	var file = FileAccess.open(path, FileAccess.WRITE)
	var bytes = var_to_bytes_with_objects(p_dict)
	for chr in "%CG3D".to_ascii_buffer():
		file.store_8(chr)
	file.store_8(0)
	file.store_64(bytes.size())
	file.store_buffer(bytes)
	file.flush()
	file = null

static func load_dict(path: String) -> Dictionary:
	var file = FileAccess.open(path, FileAccess.READ)
	if file == null:
		return {}
	for chr in "%CG3D".to_ascii_buffer():
		if file.get_8() != chr:
			return {}
	if file.get_8() != 0:
		return {}
	var size := file.get_64()
	var buf := file.get_buffer(size)
	var obj = bytes_to_var_with_objects(buf)
	if !(obj is Dictionary):
		return {}
	return obj as Dictionary

func _reset() -> void:
	dict = {}

func reset() -> void:
	_reset()

func _init():
	reset()

func _load_file() -> void:
	pass

func _save_file() -> Dictionary:
	return {}

func save_file():
	dict = _save_file()
	if automatic_mode:
		var property_values: Dictionary = {}
		var properties: Array[Dictionary] = get_property_list()
		for property in properties:
			@warning_ignore(shadowed_variable_base_class)
			var name: String = property.name
			var value
			if property.has("class_name") && property["class_name"] != "":
				continue
			if property["usage"] & PROPERTY_USAGE_SCRIPT_VARIABLE:
				value = get(name)
			else:
				continue
			property_values[name] = value
		dict["_class_variables"] = property_values
	SaveFile.save_dict(dict, Saves.current_world + basename + ".dat")

func load_file():
	reset()
	dict = SaveFile.load_dict(Saves.current_world + basename + ".dat")
	if dict == {}:
		return
	if automatic_mode:
		if dict.has("_class_variables"):
			var properties: Array[Dictionary] = get_property_list()
			var property_values: Dictionary = dict["_class_variables"]
			for property in properties:
				@warning_ignore(shadowed_variable_base_class)
				var name: String = property.name
				if property.has("class_name") && property["class_name"] != "":
					continue
				if !(property["usage"] & PROPERTY_USAGE_SCRIPT_VARIABLE):
					continue
				if property_values.has(name):
					set(name, property_values[name])
	_load_file()

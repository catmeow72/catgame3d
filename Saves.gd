extends Node

var WORLD_DIR = OS.get_user_data_dir() + "/worlds/"
var current_world = ""

func get_worlds() -> Array[Dictionary]:
	var world_dir := DirAccess.open(WORLD_DIR)
	if world_dir == null:
		return []
	var output: Array[Dictionary] = []
	for world in world_dir.get_directories():
		var file = FileAccess.open(WORLD_DIR + world + "/world.json", FileAccess.READ)
		if file == null:
			continue
		var file_str = file.get_as_text()
		var json = JSON.parse_string(file_str)
		if json is Dictionary:
			json.merge({
				"generators": []
			})
			if json.has_all(["name", "dirname"]):
				json.dirname = world
				output.append(json)
		await get_tree().process_frame
	return output

func create_world(meta: Dictionary) -> void:
	meta.mods = []
	while DirAccess.dir_exists_absolute(WORLD_DIR + meta.dirname):
		meta.dirname += "_new"
	DirAccess.make_dir_recursive_absolute(WORLD_DIR + meta.dirname)
	var file := FileAccess.open(WORLD_DIR + meta.dirname + "/world.json", FileAccess.WRITE)
	file.store_string(JSON.stringify(meta))

func load_world(world: String) -> void:
	current_world = WORLD_DIR + world + "/"
	get_tree().change_scene_to_file("res://Game/Game.tscn")

func delete(dir: DirAccess):
	dir.include_hidden = true
	dir.include_navigational = false
	for i in dir.get_files():
		print(i)
		dir.remove(i)
	for i in dir.get_directories():
		print(i)
		dir.change_dir(i)
		delete(dir)
		dir.change_dir("..")
		dir.remove(i)

func delete_world(world: String) -> void:
	Player.reset()
	var dir = DirAccess.open(WORLD_DIR + world)
	if dir != null:
		delete(dir)
		dir = null
		DirAccess.remove_absolute(WORLD_DIR + world)

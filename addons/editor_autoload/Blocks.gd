@tool
extends Node
var Enum: Dictionary = {
	air = 0,
	object = 1,
}

func _get(property: StringName):
	if property in Enum:
		return Enum[property]

func _get_property_list():
	var output: Array[Dictionary] = []
	for i in Enum.keys():
		output.append({
			"name": i,
			"type": TYPE_INT,
			"usage": PROPERTY_USAGE_READ_ONLY,
			"hint": PROPERTY_HINT_NONE
		})

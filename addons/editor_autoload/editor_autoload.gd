@tool
extends EditorPlugin


func _enter_tree():
	Engine.register_singleton("Blocks", load("res://addons/editor_autoload/Blocks.gd").new())


func _exit_tree():
	Engine.unregister_singleton("Blocks")

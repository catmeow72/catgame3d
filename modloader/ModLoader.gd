extends Node

var mods_dict: Dictionary = {}
var mods: Array[Mod] = []
var MODS_DIR = OS.get_user_data_dir() + "/mods/"
var LOADED_MODS_DIR = "res://mods/"

var floors: Array[Floor] = []
var rooms: Array[Room] = []
var hooks: Dictionary = {}
var loot_tables: Dictionary = {}

var voxel_lib: VoxelBlockyLibrary = VoxelBlockyLibrary.new()

class Stage:
	var parent: Stage = null
	var children: Array[Stage] = []
	var desc: String = ""
	var operation: String = ""
	var max: int = 0
	var value: int = 0
	var done: bool = false
	var depth: int = 0
	var last: bool = false
	func current(from_parent: bool = false) -> Stage:
		if !from_parent && parent != null && !done:
			return parent.current()
		if done && parent != null:
			return parent
		if children.is_empty():
			return self
		else:
			var last_child = children.back()		
			return last_child.current(true)
	
	func modify(dict: Dictionary):
		for key in dict.keys():
			if key != "parent" || key != "children" || key != "done":
				if key in self:
					self[key] = dict[key]
	
	func new_child(max: int, desc: String) -> Stage:
		var current = current()
		if !children.is_empty() && children.back() != current:
			if current.parent != null:
				return current.parent.new_child(max, desc)
		var child = Stage.new(max, desc)
		child.parent = self
		child.depth = depth + 1
		children.append(child)
		return child
	
	func mark_done():
		last = true
		value = max
		done = true
		operation = ""
	
	func _init(max: int, desc: String):
		self.max = max
		self.desc = desc

var entities: Dictionary = {}
signal stage_info_changed(stages: Array[Dictionary])
var stages: Stage = null
var prev_stage: Stage = null

func push_stage(max: int, desc: String):
	if stages == null:
		stages = Stage.new(max, desc)
	else:
		var current = stages.current()
		while current.done:
			if current.parent == null:
				break
			current = current.parent
		current.new_child(max, desc)
	await send_stage_info()

func modify_stage(new_data: Dictionary):
	var stage = stages.current()
	stage.modify(new_data)
	await send_stage_info()

var lines: int = 0
var prev_depth = -1
func print_stages():
	var current = stages.current()
	var depth = current.depth
	var desc = current.desc
	var str = " "
	var operation = current.operation
	var actual_depth: int = depth
	var last: bool = false
	var max: int = current.max
	var value: int = current.value
	if depth < prev_depth || current.done:
		if !current.children.is_empty() && current.children.back().done:
			if depth < prev_depth:
				depth += 1
			operation = ""
			desc = "Done"
			last = true
			max = 0
			value = 0
	for j in range(max(depth - 1, 0)):
		str += "│ "
	if prev_stage == null:
		str += "┌"
	elif prev_depth < depth:
		str += "├─┬"
	else:
		if depth != 0:
			str += "│ "
		if last:
			str += "└"
		else:
			str += "├"
	str += "───"
	str += desc
	if operation != "":
		str += " - " + operation
	if max == 0:
		str += "!" if last else "..."
	else:
		str += " - %d/%d (%d%%)..." % [value, max, int((float(value) / float(max)) * 100)]
	print(str)
	prev_stage = current
	prev_depth = actual_depth

func send_stage_info():
	var str = ""
	print_stages()
	var timer: float = 0
	while timer < 0.125:
		await get_tree().process_frame
		timer += get_process_delta_time()
	stage_info_changed.emit(stages)

func pop_stage():
	stages.current().mark_done()
	await send_stage_info()

func _enter_tree():
	await push_stage(0, "Loading mods")
	voxel_lib.voxel_count = 2
	var air = voxel_lib.create_voxel(0, "air")
	var obj = voxel_lib.create_voxel(1, "object")
	air.geometry_type = VoxelBlockyModel.GEOMETRY_NONE
	obj.geometry_type = VoxelBlockyModel.GEOMETRY_NONE
	obj.collision_aabbs = [
		AABB(Vector3.ZERO, Vector3.ONE)
	]
	await push_stage(0, "Loading mod files")
	if !DirAccess.dir_exists_absolute(MODS_DIR):
		DirAccess.make_dir_recursive_absolute(MODS_DIR)
	var dir := DirAccess.open(MODS_DIR)
	var files := dir.get_files() as Array[String]
	files.filter(func(value: String):
		return value.ends_with(".pck") || value.ends_with(".zip")
	)
	await modify_stage({
		"max": files.size()
	})
	var i: int = 0
	for filename in files:
		var filepath: String = MODS_DIR + filename
		await modify_stage({
			"operation": filename,
			"value": i
		})
		ProjectSettings.load_resource_pack(filepath, false)
		i += 1
	await pop_stage()

func _ready():
	await push_stage(0, "Initializing mods")
	var tree = get_tree()
	var dir := DirAccess.open(LOADED_MODS_DIR)
	dir.include_hidden = true
	dir.include_navigational = false
	var mod_dirs = dir.get_directories()
	await modify_stage({
		"max": mod_dirs.size()
	})
	var i: int = -1
	for mod_dirname in mod_dirs:
		i += 1
		await modify_stage({
			"operation": mod_dirname,
			"value": i
		})
		dir.change_dir(mod_dirname)
		var metafile := FileAccess.open(dir.get_current_dir() + "/metadata.json", FileAccess.READ)
		if metafile == null:
			dir.change_dir("..")
			continue
		var meta = JSON.parse_string(metafile.get_as_text()) as Dictionary
		if !meta.has("_version"):
			dir.change_dir("..")
			continue
		var metaversion = int(meta._version)
		var mod := load(dir.get_current_dir() + "/mod.gd").new() as Mod
		if mod == null:
			dir.change_dir("..")
			continue
		match metaversion:
			0:
				mod.name = meta.name
				mod.modid = meta.modid
		mods_dict[mod.modid] = mod
		await tree.process_frame
	await pop_stage()
	mods = mods_dict.values()
	mods.sort_custom(func(a, b):
		if a is Mod && b is Mod:
			var mod_a = a as Mod
			var mod_b = b as Mod
			var rank_a = mod_a._get_priority()
			var rank_b = mod_b._get_priority()
			if rank_a == rank_b:
				rank_b += mod_a.modid.nocasecmp_to(mod_b.modid)
			return rank_a > rank_b
		return false
	)
	await _register_content()
	await _register_hooks()
	await pop_stage()
	print("Done!")
	print("Mods: ", mods_dict.keys())

func register_block(modid: String, id: String, block: Block) -> void:
	voxel_lib.voxel_count += 1
	var model := voxel_lib.create_voxel(voxel_lib.voxel_count - 1, modid + ":" + id)
	model.geometry_type = VoxelBlockyModel.GEOMETRY_CUSTOM_MESH if block.mesh != null else VoxelBlockyModel.GEOMETRY_NONE
	if block.mesh != null:
		model.custom_mesh = block.mesh
		model.material_override_0 = block.material
	model.collision_aabbs = block.collision_aabbs
	Blocks.Enum[modid + ":" + id] = model.get_id()
	await modify_stage({
		"operation": "Registered block '%s'" % [model.voxel_name]
	})

func register_entity(modid: String, id: String, entity: PackedScene) -> void:
	var fullid = modid + ":" + id
	var tmp = entity.instantiate()
	if tmp is Entity:
		entities[fullid] = entity
	tmp.queue_free()
	await modify_stage({
		"operation": "Registered entity '%s'" % [fullid]
	})

func register_floor(modid: String, p_floor: Floor) -> void:
	floors.append(p_floor)
	await modify_stage({
		"operation": "Registered floor from '%s'" % [modid]
	})

func register_room(modid: String, room: Room) -> void:
	rooms.append(room)
	await modify_stage({
		"operation": "Registered room from mod '%s'" % [modid]
	})

func register_hook(modid: StringName, id: StringName, hook: Callable) -> void:
	if hooks.has(id) && typeof(hooks[id]) == TYPE_ARRAY:
		hooks[id].append(hook)
	else:
		hooks[id] = [hook]
	await modify_stage({
		"operation": "Registered function from mod '%s' to hook '%s'" % [modid, id]
	})

func register_item(modid: String, id: String, item: ItemData) -> void:
	ItemRegistry.register(modid + ":" + id, item)
	await modify_stage({
		"operation": "Registered '%s:%s'" % [modid, id]
	})

func register_loot_table(modid: String, id: String, loot: LootTable) -> void:
	loot_tables[modid + ":" + id] = loot
	await modify_stage({
		"operation": "Registered '%s:%s'" % [modid, id]
	})

func run_hook(id: StringName, args: Array) -> Array:
	var output := []
	if hooks.has(id):
		for hook in hooks[id]:
			hook = hook as Callable
			output.append(hook.callv(args))
	return output

func _register_content():
	await push_stage(mods.size(), "Registering content")
	var tree = get_tree()
	var i: int = 0
	for mod in mods:
		await modify_stage({
			"value": i
		})
		await push_stage(0, mod.name)
		mod._register()
		await pop_stage()
		await tree.process_frame
		i += 1
	await pop_stage()

func _register_hooks():
	await push_stage(mods.size(), "Registering hooks")
	var tree = get_tree()
	var i: int = 0
	for mod in mods:
		await modify_stage({
			"value": i
		})
		await push_stage(0, mod.name)
		mod._register_hooks()
		await pop_stage()
		await tree.process_frame
		i += 1
	await pop_stage()

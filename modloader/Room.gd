class_name Room extends RefCounted
var channel: int
const SIZE = Vector3i(16, 8, 16) 

func _pre_modify(_buffer: VoxelBuffer, _p_floor: Floor, _pos_in_room: Vector3i, _buf_pos: Vector3i) -> void:
	pass

func _modify_above(_buffer: VoxelBuffer, _p_floor: Floor, _pos_in_room: Vector3i, _buf_pos: Vector3i) -> void:
	pass

func _post_modify(_buffer: VoxelBuffer, _p_floor: Floor, _pos_in_room: Vector3i, _buf_pos: Vector3i) -> void:
	pass

func _valid(_p_floor: Floor, _y: int) -> bool:
	return true

func set_object(buffer: VoxelBuffer, buf_pos: Vector3i, id: String):
	buffer.set_voxel_metadata(buf_pos, {
		"id": id,
		"data": {}
	})
	buffer.set_voxel_v(Blocks.object, buf_pos, channel)

func set_voxel(buffer: VoxelBuffer, buf_pos: Vector3i, id: StringName):
	buffer.set_voxel_v(Blocks[id], buf_pos, channel)

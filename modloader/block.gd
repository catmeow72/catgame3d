class_name Block extends Resource

@export
var material: Material = StandardMaterial3D.new()

@export
var mesh: Mesh = BoxMesh.new()

@export_color_no_alpha
var color: Color = Color.WHITE

@export
var collision_aabbs: Array[AABB] = [AABB(Vector3(0, 0, 0), Vector3(1, 1, 1))]


class_name Mod extends Resource

var name: String
var modid: String
var floors: Array[Floor]
var rooms: Array[Room]

func _get_priority() -> int:
	return 100

func register_entity(id: String, entity: PackedScene):
	ModLoader.register_entity(modid, id, entity)

func register_block(id: String, block: Block):
	ModLoader.register_block(modid, id, block)

func register_floor(floor: Floor):
	floors.append(floor)
	ModLoader.register_floor(modid, floor)

func register_room(room: Room):
	rooms.append(room)
	ModLoader.register_room(modid, room)

func register_item(id: String, item: ItemData):
	ModLoader.register_item(modid, id, item)

func register_loot_table(id: String, loot: LootTable):
	ModLoader.register_loot_table(modid, id, loot)

func register_hook(id: StringName, hook: Callable):
	ModLoader.register_hook(modid, id, hook)

func _register() -> void:
	pass

func _register_hooks() -> void:
	pass

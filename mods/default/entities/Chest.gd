extends Entity

@onready var animations: AnimationPlayer = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	data.merge({
		"open": false,
	})
	if data.open:
		animations.play("Open", 0, 1.0, true)

func interact():
	if !data.open:
		animations.play("Open")
		var loot = ModLoader.loot_tables["default:chest"]
		Player.inventory.add_item(loot.get_output())
		data.open = true
		update()

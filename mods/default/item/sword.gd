extends ItemData

func _init():
	texture = preload("res://mods/default/textures/sword.png")
	display_name = "Sword"
	stack_size = 1

func _use_item(_item, _owner):
	return ITEM_USE_RESULT_NONE

extends Mod

func _get_priority() -> int:
	return 10

func _register() -> void:
	register_room(preload("res://mods/default/rooms/ChestRoom.gd").new())
	register_room(preload("res://mods/default/rooms/Hallway.gd").new())
	register_room(preload("res://mods/default/rooms/Stairs.gd").new())
	register_block("darkness", preload("res://mods/default/blocks/darkness.tres"))
	register_block("bricks", preload("res://mods/default/blocks/bricks.tres"))
	register_block("sand", preload("res://mods/default/blocks/sand.tres"))
	register_block("wood", preload("res://mods/default/blocks/wood.tres"))
	register_item("sword", preload("res://mods/default/item/sword.gd").new())
	register_loot_table("chest", preload("res://mods/default/loot/chest.tres"))
	register_entity("chest", preload("res://mods/default/entities/Chest.tscn"))
	register_entity("light", preload("res://mods/default/entities/light.tscn"))
	register_floor(preload("res://mods/default/floors/0.tres"))
	register_floor(preload("res://mods/default/floors/1.tres"))
	register_floor(preload("res://mods/default/floors/2.tres"))

func _register_hooks() -> void:
	register_hook("post_block_generate", _post_block_generate)

func _post_block_generate(out_buffer, floor, pos, bufpos, channel):
	var size = Room.SIZE
	var door_pos_x = size.x / 2
	var door_pos_z = size.z / 2
	if pos.x == door_pos_x && pos.z == door_pos_z && pos.y == size.y - 2:
		out_buffer.set_voxel_metadata(bufpos, {
			"id": "default:light",
			"data": {}
		})
		out_buffer.set_voxel_v(Blocks.object, bufpos, channel)

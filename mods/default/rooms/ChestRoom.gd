extends Room

func _post_modify(buffer: VoxelBuffer, _p_floor: Floor, pos_in_room: Vector3i, buf_pos: Vector3i) -> void:
	@warning_ignore(integer_division, integer_division)
	if pos_in_room.x == SIZE.x / 2 && pos_in_room.y == 0 && pos_in_room.z == SIZE.z / 2:
		set_object(buffer, buf_pos, "default:chest")

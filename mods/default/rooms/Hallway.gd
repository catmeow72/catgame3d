extends Room

func _pre_modify(buffer: VoxelBuffer, p_floor: Floor, pos_in_room: Vector3i, buf_pos: Vector3i) -> void:
	@warning_ignore(integer_division, integer_division)
	if pos_in_room.x != SIZE.x / 2 && pos_in_room.z != SIZE.z / 2 && pos_in_room.y < SIZE.y - 1:
		set_voxel(buffer, buf_pos, p_floor.wall)

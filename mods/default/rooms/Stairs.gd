extends Room

func _post_modify(buffer: VoxelBuffer, p_floor: Floor, pos: Vector3i, buf_pos: Vector3i) -> void:
	if pos.z == 1 && pos.x > 0 && pos.x - 1 <= pos.y:
		if pos.x - 1 == pos.y:
			set_voxel(buffer, buf_pos, p_floor.stairs)
		else:
			set_voxel(buffer, buf_pos, "air")

func _modify_above(buffer: VoxelBuffer, _p_floor: Floor, pos: Vector3i, buf_pos: Vector3i) -> void:
	if pos.z == 1 && pos.x > 0 && pos.x <= SIZE.y && pos.y < SIZE.y - 1:
		set_voxel(buffer, buf_pos, "air")
